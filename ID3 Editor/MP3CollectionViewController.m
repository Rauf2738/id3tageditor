//  CollectionViewController.m
//  ID3 Editor
//
//  Created by Readdle on 28.06.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "MP3CollectionViewController.h"
#import "CollectionViewCell.h"
#import "TagManager.h"
#import "TagManagingViewController.h"
#import "MP3Tags.h"
#import "Image.h"

@interface MP3CollectionViewController ()
-(UIImage*)imageFilePathToSave:(UIImage*)image;
@property TagManager *tagManager;
@end

@implementation MP3CollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    _tagManager = [TagManager sharedInstance];
    [self.collectionView registerNib:[UINib nibWithNibName:@"CollectionViewCell" bundle:nil] forCellWithReuseIdentifier:reuseIdentifier];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_dataSource numberOfMP3FilesPaths];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CollectionViewCell * returnCell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    MP3Tags *tag = [_tagManager readTagsForMP3FileAtPath:[_dataSource mp3FilePathAtIndex:indexPath.item]];
    
    returnCell.title.text = tag.title;
    returnCell.artist.text = tag.artist;
    returnCell.album.text = tag.album;
    [returnCell.cover setImage:tag.cover.image];
    return returnCell;
}

#pragma mark <UICollectionViewDelegate>

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    TagManagingViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"TagManagingViewController"];
    vc.mp3Tags = [_tagManager readTagsForMP3FileAtPath:[_dataSource mp3FilePathAtIndex:indexPath.item]];
    vc.mp3FilePath = [_dataSource mp3FilePathAtIndex:indexPath.item];
    vc.delegate = self;
    [self presentViewController:vc animated:YES completion:nil];
}


//DELEGATE
-(void)saveTags:(MP3Tags*)tag MP3FilePath:(NSString*)filePath{
    tag.cover.image = [self imageFilePathToSave:tag.cover.image];
    [_tagManager setTags:tag forMP3FileAtPath:filePath];
    [self.collectionView reloadItemsAtIndexPaths: [[NSArray alloc] initWithObjects:[NSIndexPath indexPathForRow:[_dataSource indexOfMP3FilePath:filePath] inSection:0], nil]];
}

-(void)removeTags:(NSString*)filePath{
    [_tagManager removeTags:filePath];
    [self.collectionView reloadItemsAtIndexPaths: [[NSArray alloc] initWithObjects:[NSIndexPath indexPathForRow:[_dataSource indexOfMP3FilePath:filePath] inSection:0], nil]];
}

-(UIImage*)imageFilePathToSave:(UIImage*)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    CGRect rect = CGRectMake(0, 0, 256, 256);
    if(actualWidth >= 256 && actualHeight >= 256){
        if(actualWidth > actualHeight){
            float ratio=256/actualWidth;
            actualHeight = actualHeight*ratio;
            rect = CGRectMake(0.0, 0.0, 256, actualHeight);
        }
        else {
            float ratio=256/actualHeight;
            actualWidth = actualWidth*ratio;
            rect = CGRectMake(0.0, 0.0, actualWidth, 256);
        }
    }
    else{
        if(actualWidth > actualHeight){
            float ratio=64/actualWidth;
            actualHeight = actualHeight*ratio;
            rect = CGRectMake(0.0, 0.0, 64, actualHeight);
        }
        else {
            float ratio=64/actualHeight;
            actualWidth = actualWidth*ratio;
            rect = CGRectMake(0.0, 0.0, actualWidth, 64);
        }
    }
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 1.0);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    
    return img;
}

@end

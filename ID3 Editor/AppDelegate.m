//
//  AppDelegate.m
//  ID3 Editor
//
//  Created by Readdle on 6/26/17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "AppDelegate.h"
#import "TagManager.h"
#import "DataSourceFromFilePath.h"
@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString * myfile = [mainBundle pathForResource:@"Сопрано" ofType:@"mp3"];
    NSLog(@"%@", myfile);
    NSMutableArray *mp3FilePaths = [[NSMutableArray alloc] initWithObjects: @"/Users/readdle/Desktop/ID3 Editor/ID3 Editor/Время-Мираж.mp3", @"/Users/readdle/Desktop/ID3 Editor/ID3 Editor/Сопрано.mp3", @"/Users/readdle/Desktop/ID3 Editor/ID3 Editor/01 Твои Глаза Туманы (PrimeMusic.cc).mp3", @"/Users/readdle/Desktop/ID3 Editor/ID3 Editor/01 Rise Up (PrimeMusic.cc).mp3", nil];
    
    
    DataSourceFromFilePath* dataSourceView = [[DataSourceFromFilePath alloc] init:mp3FilePaths];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    MP3CollectionViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"MP3CollectionViewController"];
    vc.dataSource = dataSourceView;
    [self.window makeKeyAndVisible];
    [[self window].rootViewController presentViewController:vc animated:YES completion:NULL];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

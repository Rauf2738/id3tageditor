//
//  Image.m
//  ID3 Editor
//
//  Created by Readdle on 09.07.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "Image.h"


@implementation Image

-(id)init{
    self = [super init];
    return self;
}

-(id)init:(UIImage*)image{
    self = [super init];
    self.image = image;
    return self;
}

@end

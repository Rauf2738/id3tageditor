//
//  CollectionViewCell.m
//  ID3 Editor
//
//  Created by Readdle on 28.06.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "CollectionViewCell.h"

@implementation CollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    //self.cover.layer.masksToBounds = true;
    self.cover.layer.cornerRadius = 15.0;
}

@end

//
//  ViewController.m
//  ID3 Editor
//
//  Created by Readdle on 6/26/17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "ViewController.h"
#import "Music.h"
#import "MusicCollectionCellCollectionViewCell.h"

@interface ViewController ()

@end

@implementation ViewController{
    Music *music;
    NSArray *tags;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    music = [[Music alloc] init];
    tags = [music getMusictags];
    NSLog(@"Array: %@", tags[0]);
    [self.collectionView registerClass:[MusicCollectionCellCollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
}
- (IBAction)pressButton:(id)sender {
    tags = [music getMusictags];
    NSLog(@"Array: %@", tags[0]);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MusicCollectionCellCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.title = tags[0];
    cell.artist = tags[1];
    cell.album = tags[2];
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

//
//  SelectedMusic.h
//  ID3 Editor
//
//  Created by Readdle on 28.06.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MP3Tags;

@protocol TagsManagerProtocol

-(void)saveTags:(MP3Tags*)tag MP3FilePath:(NSString*)filePath;
-(void)removeTags:(NSString*)filePath;

@end

@interface TagManagingViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *coverImage;
@property (weak, nonatomic) IBOutlet UITextField *songTitleTextField;
@property (weak, nonatomic) IBOutlet UITextField *artistTextField;
@property (weak, nonatomic) IBOutlet UITextField *albumTextField;
@property MP3Tags *mp3Tags;
@property NSString *mp3FilePath;

@property (weak, nonatomic) id <TagsManagerProtocol> delegate;

@end

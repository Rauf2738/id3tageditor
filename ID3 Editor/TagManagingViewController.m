//
//  SelectedMusic.m
//  ID3 Editor
//
//  Created by Readdle on 28.06.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "TagManagingViewController.h"
#import "TagManager.h"
#import "MP3Tags.h"
#import "Image.h"

@interface TagManagingViewController()

@end

@implementation TagManagingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(_mp3Tags != nil){
        _coverImage.image = _mp3Tags.cover.image;
        _songTitleTextField.text = _mp3Tags.title;
        _artistTextField.text = _mp3Tags.artist;
        _albumTextField.text = _mp3Tags.album;
    }
    
    UITapGestureRecognizer *singleTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapping:)];
    [_coverImage addGestureRecognizer:singleTap];
}

-(void)singleTapping:(UIGestureRecognizer *)recognizer {
    UIImagePickerController *imagePicker = [UIImagePickerController new];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePicker animated:true completion:nil];
}

- (IBAction)turnBack:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
    NSString * storyboardName = @"Main";
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle: nil];
    TagManagingViewController * vc = [storyboard instantiateViewControllerWithIdentifier:@"TagManagingViewController"];
    [self presentViewController:vc animated:YES completion:nil];
}


- (IBAction)saveTag:(id)sender {
    if(_mp3Tags.title != _songTitleTextField.text){
        _mp3Tags.title = _songTitleTextField.text;
    }
    if(_mp3Tags.artist != _artistTextField.text){
        _mp3Tags.artist = _artistTextField.text;
    }
    if(_mp3Tags.album != _albumTextField.text){
        _mp3Tags.album = _albumTextField.text;
    }
    if(_mp3Tags.cover.image != _coverImage.image){
        _mp3Tags.cover.image = _coverImage.image;
    }
    [self.delegate saveTags:_mp3Tags MP3FilePath:_mp3FilePath];
}

-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    _coverImage.image = info[UIImagePickerControllerOriginalImage];
    _mp3Tags.cover.mimeType = [[info[UIImagePickerControllerReferenceURL] lastPathComponent] substringFromIndex:6];
    [picker dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)deleteTag:(id)sender {
    [self.delegate removeTags:_mp3FilePath];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

//
//  Image.h
//  ID3 Editor
//
//  Created by Readdle on 09.07.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Image : NSObject

-(id)init;
-(id)init:(UIImage*)image;

@property UIImage *image;
@property NSString *mimeType;

@end

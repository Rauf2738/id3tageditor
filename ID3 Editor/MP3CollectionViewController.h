//
//  CollectionViewController.h
//  ID3 Editor
//
//  Created by Readdle on 28.06.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TagManagingViewController.h"


@protocol Mp3DataSource
-(NSInteger)numberOfMP3FilesPaths;
-(NSString*)mp3FilePathAtIndex:(NSInteger)indexPath;
-(NSUInteger)indexOfMP3FilePath:(NSString*)filePath;

@end

@interface MP3CollectionViewController : UICollectionViewController <TagsManagerProtocol>

@property id <Mp3DataSource> dataSource;

@end



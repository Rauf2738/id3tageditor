//
//  DataSourceFromFilePath.m
//  ID3 Editor
//
//  Created by Readdle on 01.07.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "DataSourceFromFilePath.h"

@interface DataSourceFromFilePath()

@property NSMutableArray *musicFilePaths;

@end

@implementation DataSourceFromFilePath

-(id)init:(NSMutableArray *)mp3FilePaths{
    self = [super init];
    if(mp3FilePaths != nil){
        self.musicFilePaths = mp3FilePaths;
    }
    return self;
}

-(NSInteger)numberOfMP3FilesPaths{
    return _musicFilePaths.count;
}

- (NSUInteger)indexOfMP3FilePath:(NSString *)filePath{
    return [_musicFilePaths indexOfObject:filePath];
}

-(NSString*)mp3FilePathAtIndex :(NSInteger)indexPath{
    return _musicFilePaths[indexPath];
}

@end

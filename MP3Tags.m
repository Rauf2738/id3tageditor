//
//  MP3Tags.m
//  ID3 Editor
//
//  Created by Readdle on 29.06.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "MP3Tags.h"
#import "Image.h"

@implementation MP3Tags

-(id)init{
    self = [super init];
    self.cover = [Image new];
    return self;
}

-(void)setCoverAtImage:(UIImage*)image{
    _cover = [[Image alloc] init:image];
}

@end

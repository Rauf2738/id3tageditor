//
//  TagManager.m
//  ID3 Editor
//
//  Created by Readdle on 29.06.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import "TagManager.h"
#import "MP3Tags.h"
#import "id3v2lib.h"
#import "Image.h"

typedef NS_ENUM(NSUInteger, ID3TagEncodingType) {
    ID3TagWindowsCP1251Type = 0,
    ID3TagUTF16Type = 1,
    ID3TagUTF16BigEndianType = 2,
    ID3TagUTF8Type = 3,
};

@interface TagManager()
-(id)init;
@end

@implementation TagManager

+ (instancetype)sharedInstance {
    static id _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

- (id)init{
    self = [super init];
    _cacheController = [NSCache new];
    return self;
}


- (NSStringEncoding)encodingForTextContent:(ID3v2_frame_text_content)textContent
{
    NSStringEncoding result;
    switch (textContent.encoding) {
        case ID3TagWindowsCP1251Type:
            result = NSWindowsCP1251StringEncoding;
            NSLog(@"Windows1251");
            break;
        case ID3TagUTF16Type:
            result = NSUTF16StringEncoding;
            NSLog(@"UTF-16");
            break;
        case ID3TagUTF16BigEndianType:
            result = NSUTF16BigEndianStringEncoding;
            NSLog(@"UTF-16 BE");
            break;
        case ID3TagUTF8Type:
            result = NSUTF8StringEncoding;
            NSLog(@"UTF-8");
            break;
        default:
            result = NSUTF8StringEncoding;
            NSLog(@"Default");
            break;
    }
    return result;
}

- (MP3Tags *)readTagsForMP3FileAtPath:(NSString *)filePath
{
    MP3Tags *tags = [MP3Tags new];
    if(![_cacheController objectForKey:filePath]){
        ID3v2_tag* tag = load_tag([filePath UTF8String]);
        NSLog(@"%@", filePath);
        
        ///////////////Title//////////////
        ID3v2_frame* title_frame = tag_get_title(tag); // Get the title frame
        ID3v2_frame_text_content* title_content = parse_text_frame_content(title_frame);
        if(title_content != nil){
            NSString *title_text = [NSString new];
            title_text = [[NSString alloc] initWithBytes:title_content->data length:title_content->size encoding: [self encodingForTextContent:*title_content]];
            NSLog(@"%c", title_content->encoding);
            tags.title = title_text;
        }
        
        ///////////////Artist//////////////
        ID3v2_frame* artist_frame = tag_get_artist(tag); // Get the full artist frame
        ID3v2_frame_text_content* artist_content = parse_text_frame_content(artist_frame);
        if(artist_content != nil){
            NSString *artist_text = [NSString new];
            artist_text = [[NSString alloc] initWithBytes:artist_content->data length:artist_content->size encoding:[self encodingForTextContent:*artist_content]];
            tags.artist = artist_text;
        }
        
        ///////////////Album//////////////
        ID3v2_frame* album_frame = tag_get_album(tag); // Get the album frame
        ID3v2_frame_text_content* album_content = parse_text_frame_content(album_frame);
        if(album_content != nil){
            NSString *album_text = [NSString new];
            album_text = [[NSString alloc] initWithBytes:album_content->data length:album_content->size encoding:[self encodingForTextContent:*album_content]];
            tags.album = album_text;
        }
        
        ///////////////Genre//////////////
        ID3v2_frame* genre_frame = tag_get_genre(tag); // Get the genre frame
        ID3v2_frame_text_content* genre_content = parse_text_frame_content(genre_frame);
        if(genre_content != nil){
            NSString *genre_text = [NSString new];
            genre_text = [[NSString alloc] initWithBytes:genre_content->data length:genre_content->size encoding:[self encodingForTextContent:*genre_content]];
            tags.genre = genre_text;
        }
        
        ///////////////Cover//////////////
        ID3v2_frame* cover_frame = tag_get_album_cover(tag); // Get the genre frame
        ID3v2_frame_apic_content* cover_content = parse_apic_frame_content(cover_frame);
        if(cover_content != nil){
            NSData *image_data = [NSData dataWithBytes:cover_content->data length:cover_content->picture_size];
            tags.cover.image = [[UIImage new] initWithData:image_data];
        }
        [_cacheController setObject:tags forKey:filePath];
    }
    else {
        tags = [_cacheController objectForKey:filePath];
    }
    return tags;
}

- (void)setTags:(MP3Tags *)tags forMP3FileAtPath:(NSString *)filePath{
    ID3v2_tag* tag = load_tag([filePath UTF8String]); // Load the full tag from the file
    
    if(tag == NULL)
    {
        tag = new_tag();
    }
    
    
    // Set the new info
    if(tags.title != nil){
        tag_set_title((char*)[tags.title UTF8String], 0, tag);
    }
    if(tags.artist != nil){
        tag_set_artist((char*)[tags.artist UTF8String], 0, tag);
    }
    if(tags.album != nil){
        tag_set_album((char*)[tags.album UTF8String], 0, tag);
    }
    if(tags.genre != nil){
        tag_set_genre((char*)[tags.genre UTF8String], 0, tag);
    }
    
    // Get NSData from UIImage and set the ID3 album' cover
    NSData *album_cover_bytes = [NSData new];
    char *mimetype;
    if([tags.cover.mimeType  isEqual: @"JPG"]){
        album_cover_bytes = UIImageJPEGRepresentation(tags.cover.image, 1);
        mimetype = "image/jpeg";
    }
    else {
        album_cover_bytes = UIImagePNGRepresentation(tags.cover.image);
        mimetype = "image/png";
    }
    int picture_size = (int)album_cover_bytes.length;
    if(picture_size != 0){
        tag_set_album_cover_from_bytes((char *)[album_cover_bytes bytes], mimetype, picture_size, tag);
    }
    
    set_tag([filePath UTF8String], tag);
    [_cacheController setObject:tags forKey:filePath];
}

-(void) removeTags:(NSString *)filePath{
    remove_tag([filePath UTF8String]);
    [_cacheController removeObjectForKey:filePath];
}


@end

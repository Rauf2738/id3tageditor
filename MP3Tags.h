//
//  MP3Tags.h
//  ID3 Editor
//
//  Created by Readdle on 29.06.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class Image;

@interface MP3Tags : NSObject

@property NSString *title;
@property NSString *artist;
@property NSString *album;
@property NSString *genre;
@property Image *cover;
-(void)setCoverAtImage:(UIImage*)image;
-(id)init;
@end

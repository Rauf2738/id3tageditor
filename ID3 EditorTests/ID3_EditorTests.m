//
//  ID3_EditorTests.m
//  ID3 EditorTests
//
//  Created by Readdle on 6/26/17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "TagManager.h"
#import "MP3Tags.h"
#import "Image.h"
@interface ID3_EditorTests : XCTestCase

@end

@implementation ID3_EditorTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

//Write and Read Tags Test
- (void)testWriteAndReadTags {
    MP3Tags *mp3Tags = [[MP3Tags alloc] init];
    [mp3Tags setTitle:@"Title"];
    [mp3Tags setArtist:@"Artist"];
    [mp3Tags setAlbum:@"Album"];
    [mp3Tags setGenre:@"Genre"];
    [mp3Tags setCover:[[UIImage alloc] initWithContentsOfFile:@"/Users/readdle/Desktop/ID3 Editor/ID3 EditorTests/testImage.png"]];
    
    TagManager *tagManager = [TagManager sharedInstance];
    [tagManager setTags:mp3Tags forMP3FileAtPath:@"/Users/readdle/Desktop/ID3 Editor/ID3 EditorTests/testMp3.mp3"];
    
    MP3Tags *readMp3Tags = [[MP3Tags alloc] init];
    readMp3Tags = [tagManager readTagsForMP3FileAtPath: @"/Users/readdle/Desktop/ID3 Editor/ID3 EditorTests/testMp3.mp3"];
    
    XCTAssertEqualObjects(mp3Tags.title, readMp3Tags.title);
    XCTAssertEqualObjects(mp3Tags.artist, readMp3Tags.artist);
    XCTAssertEqualObjects(mp3Tags.album, readMp3Tags.album);
    XCTAssertEqualObjects(mp3Tags.genre, readMp3Tags.genre);
    XCTAssertEqualObjects(UIImagePNGRepresentation(mp3Tags.cover.image),UIImagePNGRepresentation(readMp3Tags.cover.image));
}


//Set Image Test (64 * 64)
-(void)testImageSet64{
    MP3Tags *mp3Tags = [[MP3Tags alloc] init];
    [mp3Tags setCover:[[UIImage alloc] initWithContentsOfFile:@"/Users/readdle/Desktop/ID3 Editor/ID3 EditorTests/202*202.png"]];
    
    TagManager *tagManager = [TagManager sharedInstance];
    [tagManager setTags:mp3Tags forMP3FileAtPath:@"/Users/readdle/Desktop/ID3 Editor/ID3 EditorTests/testMp3.mp3"];
    
    MP3Tags *readMp3Tags = [[MP3Tags alloc] init];
    readMp3Tags = [tagManager readTagsForMP3FileAtPath: @"/Users/readdle/Desktop/ID3 Editor/ID3 EditorTests/testMp3.mp3"];
    
    
    float currentSize = readMp3Tags.cover.image.size.height * readMp3Tags.cover.image.scale;
    float rightSize = 64;
    XCTAssertEqual(currentSize, rightSize);
}
//Set Image Test (256 * 256)
-(void)testImageSet256{
    MP3Tags *mp3Tags = [[MP3Tags alloc] init];
    [mp3Tags setCover:[[UIImage alloc] initWithContentsOfFile:@"/Users/readdle/Desktop/ID3 Editor/ID3 EditorTests/400*400.png"]];
    
    TagManager *tagManager = [TagManager sharedInstance];
    [tagManager setTags:mp3Tags forMP3FileAtPath:@"/Users/readdle/Desktop/ID3 Editor/ID3 EditorTests/testMp3.mp3"];
    
    MP3Tags *readMp3Tags = [[MP3Tags alloc] init];
    readMp3Tags = [tagManager readTagsForMP3FileAtPath: @"/Users/readdle/Desktop/ID3 Editor/ID3 EditorTests/testMp3.mp3"];
    
    
    float currentSize = readMp3Tags.cover.image.size.height * readMp3Tags.cover.image.scale;
    float rightSize = 256;
    XCTAssertEqual(currentSize, rightSize);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end

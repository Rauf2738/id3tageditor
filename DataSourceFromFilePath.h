//
//  DataSourceFromFilePath.h
//  ID3 Editor
//
//  Created by Readdle on 01.07.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MP3CollectionViewController.h"


@interface DataSourceFromFilePath : NSObject <Mp3DataSource>

-(id)init: (NSMutableArray*)mp3FilePaths;

@end

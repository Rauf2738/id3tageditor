//
//  TagManager.h
//  ID3 Editor
//
//  Created by Readdle on 29.06.17.
//  Copyright © 2017 Readdle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class MP3Tags;

@interface TagManager : NSObject

@property NSCache *cacheController;

//Singletone
+ (instancetype)sharedInstance;

- (MP3Tags *)readTagsForMP3FileAtPath:(NSString *)filePath;
- (void)setTags:(MP3Tags *)tags forMP3FileAtPath:(NSString *)filePath;
- (void)removeTags:(NSString*)filePath;


@end
